This is a sample project that shows how to create a listview with a custom header that has a background with parallax effect.

Here is the resulting effect:

![slothFramework](http://slothdevelopers.files.wordpress.com/2014/05/parallaxeffect.gif)

The app support api level 8 and above, but **the parallax effect is only available for api level 11 and above** (Android 3.0).

This example uses the [SlothFramework](https://bitbucket.org/jewinaruto/slothframework)

![sloth_logo-03.png](https://bitbucket.org/repo/K5Eody/images/386427969-sloth_logo-03.png)